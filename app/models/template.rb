class Template < ActiveRecord::Base

	validates :name, uniqueness: true
	validates :name, presence: true

	has_many :template_item

end
