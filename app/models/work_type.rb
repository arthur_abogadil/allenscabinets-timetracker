class WorkType < ActiveRecord::Base

	validates :name, uniqueness: true
	validates :name, presence: true

	validates :average_duration, presence: true

	validates :average_duration, :numericality => { :greater_than_or_equal_to => 0 }

	
end
