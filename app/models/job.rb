class Job < ActiveRecord::Base
	has_many :work_type_item

	validates :name, uniqueness: true
	validates :name, presence: true
end
