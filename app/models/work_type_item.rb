class WorkTypeItem < ActiveRecord::Base
 
  belongs_to :job
  belongs_to :work_type
  belongs_to :user

  validates :estimated_duration, presence: true

  validates :estimated_duration, :numericality => { :greater_than_or_equal_to => 0 }


end
