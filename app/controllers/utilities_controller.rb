class UtilitiesController < ApplicationController

	def login

		@users = User.all
		@login = ""


	end


	def login2

		@user = User.find params[:id]
		
		Time.zone = "Taipei"
		@login = ""

		if request.post?

			@user = User.where(:username => params[:username], :password => params[:password]).first

			if @user != nil

				if @user.isactive == false

					flash[:notice] = 'Login Failed: User is Inactive!'
					return

				end

				session[:current_user_id] = @user.id
				session[:current_user_type] = @user.user_type
				session[:current_user_full_name] = @user.firstname + " " + @user.lastname
				flash[:notice] = 'Login Successful!'

				if @user.user_type == "Manager"
					redirect_to :controller => "admin", :action => "index"
				else
					redirect_to :controller => "staff", :action => "index"
				end
				

			else
				flash[:notice] = 'Login Failed: Invalid Username / Password combination!'
			end

		end

	end

	def logout
		reset_session
		redirect_to :action => "login"
	end


end
