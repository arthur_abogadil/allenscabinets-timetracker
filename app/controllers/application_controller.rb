class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  private

  def require_login
    unless logged_in?
      flash[:error] = "You must be logged in to access this section" 
      redirect_to new_login_url # Prevents the current action from running
    end

  end

  def redirect_to_staff
    unless session[:current_user_type] == "Manager" 
      flash[:error] = "Redirected to Staff Panel" 
      redirect_to :controller => "staff", :action => "index"
    end

  end

  def logged_in?
  	current_user = session[:current_user_id] 
    !!current_user
  end


end
