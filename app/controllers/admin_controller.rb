require "prawn"
require 'securerandom'

include ActionView::Helpers::DateHelper

class AdminController < ApplicationController


    before_filter :require_login
    before_filter :redirect_to_staff


	def index
	
		current_user = get_current_user(session[:current_user_id])
		#flash[:error] = "Welcome to the site (#{session[:current_user_type]}) #{@full_name}!"

		@work_items_completed_today =  WorkTypeItem.where(:end_datetime => Time.now.beginning_of_day..Time.now.end_of_day).count
		@work_items_on_progress = WorkTypeItem.where(:end_datetime => nil).where.not(:user_id => nil).count
		@work_items_open = WorkTypeItem.where(:user_id => nil).count
		@idle_staff =  User.where(:user_type => 'Staff', :isworking => false).count

		@worktypeitems2 = WorkTypeItem.where(:end_datetime => nil).where.not(:user_id => nil)
		@worktypeitems = WorkTypeItem.where(:end_datetime => Time.now.beginning_of_day..Time.now.end_of_day)


		@users = User.where(:isworking => false)
		@work_items = WorkTypeItem.where(:user_id => nil)



	end


	#USERS FUNCTIONS ---------------------------
	
	def users

		@users = User.all

	end

	def users_add

		if [:post, :patch].include?(request.request_method_symbol) 

	      if request.request_method_symbol == :patch
	         
	         id = params[:user_id]
	         @user = User.find id

	      else   
	        
	         @user = User.new
	         
	      end 

	      if @user.update_attributes(user_params)

	      	@user.isworking = false
	      	@user.save
	        flash[:notice] = 'User Added / Updated'
	        redirect_to action: 'users_view', id: @user.id

	        return
	      else
	        return
	        flash[:notice] = 'Error'
	      end

    	end  

		@user = User.new

	end

	def users_edit
		if [:post, :patch].include?(request.request_method_symbol) 

	      if request.request_method_symbol == :patch
	         
	         id = params[:id]
	         @user = User.find id

	      else   
	        
	         @user = User.new
	         
	      end 

	      if @user.update_attributes(user_params)
	        flash[:notice] = 'User Added / Updated'
	        redirect_to action: 'users_view', id: @user.id

	        return
	      else
	        return
	        flash[:notice] = 'Error'
	      end

    	end  

		@user = User.find params[:id]
	end

	def users_view
		@user = User.find params[:id]
	end

	def users_delete
		@user = User.find params[:id]
	end

	def users_delete_confirm
		User.delete params[:id]
		redirect_to :action => "users"
	end




	def user_params
    	params.require(:user).permit(:isworking, :lastwork, :firstname, :lastname, :username, :password, :isactive, :user_type, :notes)
  	end

	#END USERS FUNCTIONS -----------------------

	#WORK TYPES FUNCTIONS ----------------------

	def worktypes

		@worktypes = WorkType.all		

	end

	def worktypes_add

		if [:post, :patch].include?(request.request_method_symbol) 

	      if request.request_method_symbol == :patch
	         
	         id = params[:id]
	         @worktype = WorkType.find id

	      else   
	        
	         @worktype = WorkType.new
	         
	      end 

	      if @worktype.update_attributes(worktype_params)
	        flash[:notice] = 'Work Type Added / Updated'
	        redirect_to action: 'worktypes_view', id: @worktype.id

	        return
	      else
	        return
	        flash[:notice] = 'Error'
	      end

    	end  

		@worktype = WorkType.new

	end

	def worktypes_edit
		if [:post, :patch].include?(request.request_method_symbol) 

	      if request.request_method_symbol == :patch
	         
	         id = params[:id]
	         @worktype = WorkType.find id

	      else   
	        
	         @worktype = WorkType.new
	         
	      end 

	      if @worktype.update_attributes(worktype_params)
	        flash[:notice] = 'Work Type Added / Updated'
	        redirect_to action: 'worktypes_view', id: @worktype.id

	        return
	      else
	        return
	        flash[:notice] = 'Error'
	      end

    	end  

		@worktype = WorkType.find params[:id]
	end

	def worktypes_view
		@worktype = WorkType.find params[:id]
	end

	def worktypes_delete
		@worktype = WorkType.find params[:id]
	end

	def worktypes_delete_confirm
		WorkType.delete params[:id]
		redirect_to :action => "worktypes"
	end


	def worktype_params
    	params.require(:work_type).permit(:name, :average_duration, :notes)
  	end
	#END WORK TYPES FUNCTIONS ------------------

	#WORK TYPE TEMPLATES -----------------------



	#END WORK TYPE TEMPLATES -------------------

	def worktype_templates

		@worktype_templates = Template.all

	end

	def worktype_templates_add

		if [:post, :patch].include?(request.request_method_symbol) 

	      if request.request_method_symbol == :patch
	         
	         id = params[:id]
	         @worktype_template = Template.find id

	      else   
	        
	         @worktype_template = Template.new
	         
	      end 

	      if @worktype_template.update_attributes(worktype_templates_params)
	        flash[:notice] = 'Work Type Template Added / Updated'
	        redirect_to action: 'worktype_templates_view', id: @worktype_template.id

	        return
	      else
	        return
	        flash[:notice] = 'Error'
	      end

    	end  

		@worktype_template = Template.new

	end

	def worktype_templates_edit
		if [:post, :patch].include?(request.request_method_symbol) 

	      if request.request_method_symbol == :patch
	         
	         id = params[:id]
	         @worktype_template = Template.find id

	      else   
	        
	         @worktype_template = Template.new
	         
	      end 

	      if @worktype_template.update_attributes(worktype_templates_params)
	        flash[:notice] = 'Work Type Template Added / Updated'
	        redirect_to action: 'worktype_templates_view', id: @worktype_template.id

	        return
	      else
	        return
	        flash[:notice] = 'Error'
	      end

    	end  

		@worktype_template = Template.find params[:id]
	end

	def worktype_templates_view
		@worktype_template = Template.find params[:id]
	end

	def worktype_templates_delete
		@worktype_template = Template.find params[:id]
	end

	def worktype_templates_delete_confirm
		Template.delete params[:id]
		redirect_to :action => "worktype_templates"
	end

	def worktype_templates_add_work_type
		@worktype_template = Template.find params[:id]
	end

	def worktype_templates_add_work_type

		if [:post, :patch].include?(request.request_method_symbol) 

	      if request.request_method_symbol == :patch
	         
	         id = params[:id]
	         @worktype_template = Template.find id
	         @worktypeitem = TemplateItem.new

	      else   

	         id = params[:id]
	         @worktype_template = Template.find id	
	         @worktypeitem = TemplateItem.new
	         @worktypeitem.template_id = @worktype_template.id
	         
	      end 

	      if @worktypeitem.update_attributes(worktype_templates_item_params)
	        flash[:notice] = 'Work Type Item Added / Updated'
			
			#update_total_estimate(@worktypeitem.job_id)
			redirect_to action: 'worktype_templates_view', id: @worktype_template.id

	        return
	      else
	        return
	        flash[:notice] = 'Error'
	      end

    	end  

    	@worktype_template = Template.find params[:id]
		@worktypeitem = TemplateItem.new
		@worktypes = WorkType.all

	end

	def worktype_templates_edit_work_type

		if [:post, :patch].include?(request.request_method_symbol) 

	      if request.request_method_symbol == :patch
	         
	         id = params[:id]
	         @worktypeitem = TemplateItem.find params[:id]
			 @worktype_template = @worktypeitem.template

	      else   

	         id = params[:id]
	         @worktypeitem = TemplateItem.find params[:id]
			 @worktype_template = @worktypeitem.template
         
	      end 

	      if @worktypeitem.update_attributes(worktype_templates_item_params)
	        flash[:notice] = 'Work Type Item Added / Updated'

	        redirect_to action: 'worktype_templates_view', id: @worktype_template.id

	        return
	      else
	        return
	        flash[:notice] = 'Error'
	      end

    	end  

    	
		@worktypeitem = TemplateItem.find params[:id]
		@worktype_template = @worktypeitem.template
		@worktypes = WorkType.all

	end

	def worktype_templates_delete_work_type
		@worktypeitem = TemplateItem.find params[:id]
	end

	def worktype_templates_delete_confirm_work_type
		@worktypeitem = TemplateItem.find params[:id]
		@worktype_template = @worktypeitem.template.id
		TemplateItem.delete params[:id]

		redirect_to :action => "worktype_templates_view", :id => @worktype_template
	end

	def worktype_templates_item_params
    	params.require(:template_item).permit(:work_type_id, :estimated_duration, :notes)
  	end

	def worktype_templates_params
    	params.require(:template).permit(:name, :notes)
  	end

	#START JOBS --------------------------------

	def jobs

		@jobs = Job.all

	end

	def jobs_ongoing

		@jobs = Job.where(:completed => false)

	end

	def jobs_pending


	end

	def jobs_completed

		@jobs = Job.where(:completed => true)

	end

	def jobs_add

		if [:post, :patch].include?(request.request_method_symbol) 

	      if request.request_method_symbol == :patch
	         
	         id = params[:id]
	         @job = Job.find id

	      else   
	        
	         @job = Job.new
	         
	      end 

	      if @job.update_attributes(job_params)
	        flash[:notice] = 'Job Added / Updated'

	        template_id = @job.template_id
	        @template = Template.find template_id

	        @template.template_item.each do |worktype|

	        	@a = WorkTypeItem.new
	        	@a.job_id = @job.id
	        	@a.work_type_id = worktype.work_type.id
	        	@a.estimated_duration = worktype.estimated_duration
	        	@a.notes = worktype.notes
	        	@a.save  
	        	
	        end
			
			update_total_estimate(@job.id)

	        redirect_to action: 'jobs_view', id: @job.id

	        return
	      else
	        return
	        flash[:notice] = 'Error'
	      end

    	end  

		@job = Job.new
		@templates = Template.all

	end

	def job_params
    	params.require(:job).permit(:name, :template_id, :total_estimated_duration, :completed, :date_created, :notes)
  	end

  	def jobs_edit
		if [:post, :patch].include?(request.request_method_symbol) 

	      if request.request_method_symbol == :patch
	         
	         id = params[:id]
	         @job = Job.find id

	      else   
	        
	         @job = Job.new
	         
	      end 

	      if @job.update_attributes(job_params)
	        flash[:notice] = 'Job Added / Updated'
	        redirect_to action: 'jobs_view', id: @job.id

	        return
	      else
	        return
	        flash[:notice] = 'Error'
	      end

    	end  

		@job = Job.find params[:id]
	end

	def jobs_view
		
		@job = Job.find params[:id]
		@worktypeitems = WorkTypeItem.where(:job_id => params[:id])

		@link_to_pdf = create_jobs_pdf(@job, @worktypeitems, nil)

	end



	def create_jobs_pdf(job, wti, data)

		folder = "public/pdf/"
		filename = ""

		if job.nil? 
			filename = 'report_' + SecureRandom.hex + '.pdf'
		else 
			filename = 'jobs_view_' + SecureRandom.hex + '.pdf'	
		end

		Prawn::Document.generate(folder + filename) do
			
			font_size 16
			text 'ALLENS CABINETS - TIME TRACKER APP'
			font_size 10
			fill_color "888888"

			text 'As Of: ' + Time.now.strftime("%m/%d/%Y at %I:%M %p")
			stroke_horizontal_rule
    	    move_down 10

    	    x = 0 
    	    y = 670
    	    line_height = 20
    	    col_width = 80
    	    line = 1

    	    if !data.nil?

    	    	data.each do |key, value|

					font_size 10
					line += 1
					fill_color  "888888"
		    	    draw_text key, :at => [x,y - (line_height * line)]

					font_size 14
		    	    line += 1
		    	    fill_color "000000"
		    	    draw_text value, :at => [x, y - (line_height * line)]

    	    	end

    	    end

    	    if !job.nil?

	    	    date_created = job.date_created.strftime("%m/%d/%Y at %I:%M %p") + " " + time_ago_in_words(job.date_created) + " ago"
	    	    estimated_duration = job.work_type_item.sum(:estimated_duration).to_s
			    estimated_duration_hrs = '%.2f' % (job.work_type_item.sum(:estimated_duration) / 60.00)		

				if job.completed
					completed = "Yes"
				else
					completed = "No"
				end

				font_size 14
				line = 1
				fill_color  "888888"
	    	    draw_text "JOB INFO", :at => [x,y - (line_height * line)]

				font_size 10
	    	    line += 1
	    	    fill_color "888888"
	    	    draw_text "Job Name: ", :at => [x, y - (line_height * line)]

	    	    font_size 14
	    	    line += 1
				fill_color  "000000"
	    	    draw_text job.name, :at => [x,y - (line_height * line)]

	    	    font_size 10
				line += 1
				fill_color "888888"
				draw_text "Completed: ", :at => [x, y - (line_height * line)]

				font_size 14
				line += 1
				fill_color  "000000"
	    	    draw_text completed, :at => [x,y - (line_height * line)]

	    	    font_size 10
				line += 1
				fill_color "888888"
				draw_text "Date Created: ", :at => [x, y - (line_height * line)]
				
				font_size 14
				line += 1
				fill_color  "000000"
	    	    draw_text date_created, :at => [x,y - (line_height * line)]

	    	    font_size 10
	    	    line += 1
	    	    fill_color "888888"
				draw_text "Estimated Duration: ", :at => [x, y - (line_height * line)]

				font_size 14
				line += 1
				fill_color  "000000"
	    	    draw_text estimated_duration.to_s + " minutes " + '(' + estimated_duration_hrs.to_s + ' hours)', :at => [x,y - (line_height * line)]

				font_size 10
				line += 1
				fill_color "888888"
				draw_text "Notes: ", :at => [x, y - (line_height * line)]

				font_size 14
				line += 1
				fill_color  "000000"
	    	    draw_text job.notes, :at => [x,y - (line_height * line)]

	    	end

			font_size 14
			line += 2
			fill_color  "888888"
    	    draw_text "WORK ITEMS", :at => [x,y - (line_height * line)]


    	    font_size 10
			line += 2
			fill_color "888888"
			draw_text "WORK TYPE", :at => [x, y - (line_height * line)]
			draw_text "COMPLETED", :at => [x + (col_width * 1), y - (line_height * line)]
			draw_text "STAFF", :at => [x + (col_width * 2), y - (line_height * line)]
			draw_text "START / END", :at => [x + (col_width * 3), y - (line_height * line)]
			draw_text "TIME TAKEN / ESTIMATE / DIFF", :at => [x + (col_width * 5), y - (line_height * line)]

			sum = 0


			wti.each do |worktype|
	
				if (line_height * line) > 560
			
						start_new_page

						line = 1

					 	font_size 10
						
						fill_color "888888"
						draw_text "WORK TYPE", :at => [x, y - (line_height * line)]
						draw_text "COMPLETED", :at => [x + (col_width * 1), y - (line_height * line)]
						draw_text "STAFF", :at => [x + (col_width * 2), y - (line_height * line)]
						draw_text "START / END", :at => [x + (col_width * 3), y - (line_height * line)]
						draw_text "TIME TAKEN / ESTIMATE / DIFF", :at => [x + (col_width * 5), y - (line_height * line)]


				end 

				font_size 10
				line += 2
				fill_color "000000"
				draw_text worktype.work_type.name, :at => [x, y - (line_height * line)]

				if worktype.completed
                    completed = "Yes"
                else
                    completed = "No"
                end
				draw_text completed, :at => [x + (col_width * 1), y - (line_height * line)]


				if worktype.user_id.nil? 
					staff = "None"
                else
	                staff = worktype.user.username
	            end    
				draw_text staff, :at => [x + (col_width * 2), y - (line_height * line)]

				if worktype.user_id.nil?
                    start_t = "Start: None"
                	end_t = "End: None"
				else
                
                	if !worktype.start_datetime.nil?
                    	start_t =  "Start: " + worktype.start_datetime.strftime("%m/%d/%Y at %I:%M %p")
                    end 

                    if !worktype.end_datetime.nil? 
                        end_t= "End: " + worktype.end_datetime.strftime("%m/%d/%Y at %I:%M %p")
                    end
                end

				draw_text start_t, :at => [x + (col_width * 3), y - (line_height * line)]
				draw_text end_t, :at => [x + (col_width * 3), y - (line_height * (line + 1))]


				
				if worktype.end_datetime.nil? and worktype.start_datetime.nil?
                     
                    result = "None / " + worktype.estimated_duration.to_s + " / None"
                    color_result = "000000"
                    result_estimated = ""
                
                elsif !worktype.start_datetime.nil? and worktype.end_datetime.nil? 
                                    
                	a = ((Time.now - worktype.start_datetime) / 60).round
                    sum += a
                	result =  a.to_s + " / " + worktype.estimated_duration.to_s + " / "

                	if (a - worktype.estimated_duration) < 0
						color_result = "00FF00"
                    else
                        color_result = "FF0000"
                    end

                    result_estimated =  a - worktype.estimated_duration 
                        
                else

	                a = ((worktype.end_datetime - worktype.start_datetime) / 60).round
	                sum += a
	               
	                result = a.to_s + " / " +  worktype.estimated_duration.to_s + " / " 
	                
	                if (a - worktype.estimated_duration) < 0
						color_result = "00FF00"
	                else
	                    color_result = "FF0000"
	                end

	                result_estimated =  a - worktype.estimated_duration 
	                
	                          
                end

				draw_text result.to_s, :at => [x + (col_width * 5), y - (line_height * line)]

				fill_color color_result
				draw_text result_estimated.to_s, :at => [x + (col_width * 5 + 45), y - (line_height * line)]

			end

			line += 3

			if (line_height * line) > 560
			
						start_new_page
						line = 1
					 	
			end 

			
			
			sum_text = sum.to_s + " Mins or " +  ('%.2f' % (sum / 60.00)).to_s + " hours "
			font_size 12
			fill_color "000000"
			draw_text sum_text, :at => [x, y - (line_height * (line))]

			font_size 10
			fill_color "888888"
			draw_text "Total Time Accumulated", :at => [x, y - (line_height * (line + 1))]
			

			sum_estimated_duration = estimated_duration.to_s + " minutes " + 'or ' + estimated_duration_hrs.to_s + ' hours'
			font_size 12
			fill_color "000000"
			draw_text sum_estimated_duration, :at => [x + (col_width * 2), y - (line_height * (line))]

			fill_color "888888"
			draw_text "Total Time Estimated", :at => [x + (col_width * 2), y - (line_height * (line + 1))]
			
			estimated_duration = wti.sum(:estimated_duration)
			if (sum - estimated_duration) > 0 
	            color = "FF0000"
 	        else  
	            color ="00FF00"
	        end
	        
	        result =  (sum - estimated_duration).to_s + " Mins or " + ('%.2f' % ((sum - estimated_duration) / 60.00)).to_s + " hours"

	        font_size 12
	        fill_color color
	        draw_text result, :at => [x + (col_width * 4), y - (line_height * (line))]
			

			font_size 10
			fill_color "888888"
			draw_text "Total Time Difference", :at => [x + (col_width * 4), y - (line_height * (line + 1))]

		    
		end

		return  ('/pdf/' + filename)

	end

	def jobs_delete
		@job = Job.find params[:id]
	end

	def jobs_delete_confirm
		Job.delete params[:id]
		redirect_to :action => "jobs_ongoing"
	end

	def jobs_addworkitem

		if [:post, :patch].include?(request.request_method_symbol) 

	      if request.request_method_symbol == :patch
	         
	         id = params[:id]
	         @job = Job.find id
	         @worktypeitem = WorkTypeItem.new

	      else   

	         id = params[:id]
	         @job = Job.find id	
	         @worktypeitem = WorkTypeItem.new
	         @worktypeitem.job_id = @job.id
	         
	      end 

	      if @worktypeitem.update_attributes(worktypeitem_params)
	        flash[:notice] = 'Work Type Item Added / Updated'
			
			update_total_estimate(@worktypeitem.job_id)
			redirect_to action: 'jobs_view', id: @job.id

	        return
	      else
	        return
	        flash[:notice] = 'Error'
	      end

    	end  

    	@job = Job.find params[:id]
		@worktypeitem = WorkTypeItem.new
		@worktypes = WorkType.all
		@staffs = User.where(:user_type => "Staff", :isactive => true)

	end


	def jobs_editworkitem

		if [:post, :patch].include?(request.request_method_symbol) 

	      if request.request_method_symbol == :patch
	         
	         id = params[:id]
	         @worktypeitem = WorkTypeItem.find params[:id]
			 @job = @worktypeitem.job

	      else   

	         id = params[:id]
	         @worktypeitem = WorkTypeItem.find params[:id]
			 @job = @worktypeitem.job
	         
	      end 

	      if @worktypeitem.update_attributes(worktypeitem_params)
	        flash[:notice] = 'Work Type Item Added / Updated'

	        if @worktypeitem.user_id == nil
	        	
	        	@worktypeitem.start_datetime = nil
	        	@worktypeitem.end_datetime = nil
	        	@worktypeitem.save


	        end


			update_total_estimate(@worktypeitem.job_id)
		    redirect_to action: 'jobs_view', id: @job.id

	        return
	      else
	        return
	        flash[:notice] = 'Error'
	      end

    	end  

    	
		@worktypeitem = WorkTypeItem.find params[:id]
		@job = @worktypeitem.job
		@worktypes = WorkType.all
		@staffs = User.where(:user_type => "Staff", :isactive => true)

	end

	def worktypeitems_delete
		@worktypeitem = WorkTypeItem.find params[:id]
	end

	def worktypeitems_delete_confirm
		@worktypeitem = WorkTypeItem.find params[:id]
		@job_id = @worktypeitem.job.id
		WorkTypeItem.delete params[:id]

		update_total_estimate(@job_id)

		redirect_to :action => "jobs_view", :id => @job_id
	end

	def workitems_open

		@workitems = WorkTypeItem.where(:user_id => nil)

	end

	def workitems_ongoing

		@workitems = WorkTypeItem.where(:end_datetime => nil).where.not(:user_id => nil)

	end

	def workitems_completed
		@workitems = WorkTypeItem.where(:completed => true)
	end

	def worktypeitem_params
    	params.require(:work_type_item).permit(:work_type_id, :estimated_duration, :completed, :user_id, :start_datetime, :end_datetime, :notes)
  	end

	#END JOBS ----------------------------------

	#REPORTS -----------------------------------

	def reports_per_job


		@jobs = Job.where(:completed => true)


	end

	def reports_per_job_view

		@job = Job.find params[:id]
		@worktypeitems = WorkTypeItem.where(:job_id => @job.id)

		data = {'Report Type' => 'Work Items, By Job'}

		@link_to_pdf = create_jobs_pdf(nil, @worktypeitems, data)
		
	end



	def reports_per_employee
		@users = User.where(:user_type => "Staff")
	end

	def reports_per_employee_select_date
		
		@user = User.find params[:id]


	end

	def reports_per_employee_view

		if request.post?

			@start_date = params[:start_datetime].to_date
			@end_date = params[:end_datetime].to_date
			@user = User.find params[:employee_id]

			if @start_date.nil? or @end_date.nil?
				flash[:validation_error] = 'Please select start and end date/time'
				redirect_to :action => 'reports_per_employee_select_date', :id => params[:employee_id], :message => @message
				return
			end

			@worktypeitems = WorkTypeItem.where(:user_id => params[:employee_id],:completed => true, :end_datetime => @start_date.beginning_of_day..@end_date.end_of_day)
			@user = User.find params[:employee_id]

			data = {
				'Report Type' => 'Work Items, By Employee ',
				'Staff Name' =>  @user.firstname + " " + @user.lastname, 
				'Username'=> @user.username ,
				'Start Date' => @start_date, 
				'End Date' => @end_date
			}

			@link_to_pdf = create_jobs_pdf(nil, @worktypeitems, data)
		
		end

	end

	def reports_per_employee_per_job
		@users = User.where(:user_type => "Staff")
	end

	def reports_per_employee_per_job_select_date
		
		@user = User.find params[:id]
		@worktypes = WorkType.all

	end

	def reports_per_employee_per_job_view
		
		if request.post?

			@start_date = params[:start_datetime].to_date
			@end_date = params[:end_datetime].to_date

			if @start_date.nil? or @end_date.nil?
				flash[:validation_error] = 'Please select start and end date/time'
				redirect_to :action => 'reports_per_employee_per_job_select_date', :id => params[:employee_id], :message => @message
				return
			end

			@user = User.find params[:employee_id]
			@worktype = WorkType.find params[:work_type_id]
			@worktypeitems = WorkTypeItem.where(:work_type_id => params[:work_type_id], :user_id => params[:employee_id],:completed => true, :end_datetime => @start_date.beginning_of_day..@end_date.end_of_day)

			data = {
				'Report Type' => 'Work Items, By Employee - By Job Type',
				'Staff Name' =>  @user.firstname + " " + @user.lastname, 
				'Username'=> @user.username ,'Job Type' => @worktype.name,
				'Start Date' => @start_date, 
				'End Date' => @end_date
			}

			@link_to_pdf = create_jobs_pdf(nil, @worktypeitems, data)
		
		end

	end

	def reports_per_date_select_date
		
		
	end

	def reports_per_date_view
		
		if request.post?

			@start_date = params[:start_datetime].to_date
			@end_date = params[:end_datetime].to_date

			if @start_date.nil? or @end_date.nil?
				flash[:validation_error] = 'Please select start and end date/time'
				redirect_to :action => 'reports_per_date_select_date', :id => params[:employee_id], :message => @message
				return
			end

			@worktypeitems = WorkTypeItem.where(:end_datetime => @start_date.beginning_of_day..@end_date.end_of_day)

			data = {'Report Type' => 'Work Items, By Date','Start Date' => @start_date, 'End Date' => @end_date}

			@link_to_pdf = create_jobs_pdf(nil, @worktypeitems, data)

		
		end

	end


	#END REPORTS -------------------------------

	#PDF GENERATION ----------------------------

	def pdf_test()

		Prawn::Document.generate("public/pdf/hello.pdf") do
		  text "Hello World!"
		end

	end

	#END PDF GENERATION ------------------------


	#UTITLITY FUNCTIONS ------------------------

	def get_current_user(id)

		return User.find(id)

	end

	def update_total_estimate(job_id)

		@job = Job.find job_id
		@job.total_estimated_duration = @job.work_type_item.sum(:estimated_duration)
		@job.save

	end

	#END UTILITY FUNCTIONS ---------------------

end
