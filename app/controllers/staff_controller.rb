class StaffController < ApplicationController

    before_filter :require_login

	#STAFF FRONTEND ----------------------------

	def index

		#should display 
		# 1. the current job, date / time started, time passed, stop button
		# 2. list of jobs with unclaim open work items
		# 3. logout.

		if has_work?
			
			@has_work = true
			@current_work = current_work

		else

			@open_jobs = Job.where(:completed => false)	

		end	


	end

	def workitem_stop
		@current_work = WorkTypeItem.find params[:id]
	end

	def workitem_stop_confirm
		
		@time = Time.now
		@current_work = WorkTypeItem.find params[:id]
		@current_work.completed = true
		@current_work.end_datetime = @time
		@current_work.save
		@job = Job.find @current_work.job.id

		if WorkTypeItem.where(:job_id => @current_work.job.id, :completed => false).count == 0
			@job.completed = true
			@job.save
		end

		@user = User.find session[:current_user_id]
		@user.isworking = false
		@user.lastwork = @time
		@user.save



		redirect_to :controller => 'utilities', :action => 'logout'

	end

	def job_details

		@job = Job.find params[:id]

	end

	def worktype_details
		@worktype = WorkTypeItem.find params[:id]
	end

	def worktype_start
		@worktype = WorkTypeItem.find params[:id]
	end

	def worktype_start_confirm
		
		@worktype = WorkTypeItem.find params[:id]

		if !@worktype.start_datetime.nil?
			flash[:notice] = 'Start Failed: ' + @worktype.work_type.name + ' for ' + @worktype.job.name + ', has already been started by ' + @worktype.user.username + '! Please select another job!'
			redirect_to :controller => 'staff', :action => 'index'
			return
		end

		@worktype.start_datetime = Time.now
		@worktype.user_id = session[:current_user_id]
		@worktype.save

		@user = User.find session[:current_user_id]
		@user.isworking = true
		@user.save

		flash[:notice] = 'Successfully Started job [' + @worktype.work_type.name + '] for ' + @worktype.job.name + '!'
		redirect_to :controller => 'utilities', :action => 'logout'

	end

	def stats
		@jobs_completed_today =  WorkTypeItem.where(:user_id => session[:current_user_id], :completed => true, :end_datetime => DateTime.now.beginning_of_day..DateTime.now.end_of_day)
	end

	#END STAFF FRONTEND ------------------------

	#UTILITIES ---------------------------------
	def has_work?

		return (WorkTypeItem.where(:user_id => session[:current_user_id], :completed => false).count != 0)

	end

	def current_work
	
		return WorkTypeItem.where(:user_id => session[:current_user_id], :completed => false).first
	
	end

	def get_current_user

		@user = User.find session[:current_user_id]

	end
	#END UTILITIES ----------------------------

end
