class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :firstname
      t.string :lastname
      t.string :user_type
      t.string :username
      t.string :password
      t.string :notes

      t.timestamps
    end
  end
end
