class CreateJobs < ActiveRecord::Migration
  def change
    create_table :jobs do |t|
      t.text :name
      t.datetime :date_created
      t.boolean :completed
      t.integer :total_estimated_duration
      t.text :notes

      t.timestamps
    end
  end
end
