class CreateTemplateItems < ActiveRecord::Migration
  def change
    create_table :template_items do |t|
      t.references :work_type, index: true
      t.integer :duration
      t.text :notes

      t.timestamps
    end
  end
end
