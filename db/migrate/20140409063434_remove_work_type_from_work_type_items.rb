class RemoveWorkTypeFromWorkTypeItems < ActiveRecord::Migration
  def change
    remove_reference :work_type_items, :worktype, index: true
  end
end
