class CreateWorkTypeItems < ActiveRecord::Migration
  def change
    create_table :work_type_items do |t|
      t.references :job, index: true
      t.references :worktype, index: true
      t.boolean :completed
      t.integer :estimated_duration
      t.references :user, index: true
      t.datetime :start_datetime
      t.datetime :end_datetime
      t.text :notes

      t.timestamps
    end
  end
end
