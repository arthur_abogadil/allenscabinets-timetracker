class AddTemplateToTemplateItem < ActiveRecord::Migration
  def change
    add_reference :template_items, :template, index: true
  end
end
