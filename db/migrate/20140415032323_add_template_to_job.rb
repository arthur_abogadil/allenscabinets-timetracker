class AddTemplateToJob < ActiveRecord::Migration
  def change
    add_reference :jobs, :template, index: true
  end
end
