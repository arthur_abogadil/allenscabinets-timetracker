class CreateWorkTypes < ActiveRecord::Migration
  def change
    create_table :work_types do |t|
      t.string :name
      t.integer :average_duration
      t.text :notes

      t.timestamps
    end
  end
end
