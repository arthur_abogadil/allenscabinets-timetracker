class AddEstimatedDurationToTemplateItem < ActiveRecord::Migration
  def change
    add_column :template_items, :estimated_duration, :integer
  end
end
