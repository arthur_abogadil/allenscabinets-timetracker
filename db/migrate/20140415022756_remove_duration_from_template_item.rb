class RemoveDurationFromTemplateItem < ActiveRecord::Migration
  def change
    remove_column :template_items, :duration, :integer
  end
end
