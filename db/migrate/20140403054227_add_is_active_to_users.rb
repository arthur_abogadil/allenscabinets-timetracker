class AddIsActiveToUsers < ActiveRecord::Migration
  def change
    add_column :users, :isactive, :boolean
  end
end
