class AddWorkTypeToWorkTypeItems < ActiveRecord::Migration
  def change
    add_reference :work_type_items, :work_type, index: true
  end
end
