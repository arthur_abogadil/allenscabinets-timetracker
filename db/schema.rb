# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140415032323) do

  create_table "jobs", force: true do |t|
    t.text     "name"
    t.datetime "date_created"
    t.boolean  "completed"
    t.integer  "total_estimated_duration"
    t.text     "notes"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "template_id"
  end

  add_index "jobs", ["template_id"], name: "index_jobs_on_template_id"

  create_table "template_items", force: true do |t|
    t.integer  "work_type_id"
    t.text     "notes"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "template_id"
    t.integer  "estimated_duration"
  end

  add_index "template_items", ["template_id"], name: "index_template_items_on_template_id"
  add_index "template_items", ["work_type_id"], name: "index_template_items_on_work_type_id"

  create_table "templates", force: true do |t|
    t.string   "name"
    t.text     "notes"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "firstname"
    t.string   "lastname"
    t.string   "user_type"
    t.string   "username"
    t.string   "password"
    t.string   "notes"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "isactive"
    t.boolean  "isworking"
    t.datetime "lastwork"
  end

  create_table "work_type_items", force: true do |t|
    t.integer  "job_id"
    t.boolean  "completed"
    t.integer  "estimated_duration"
    t.integer  "user_id"
    t.datetime "start_datetime"
    t.datetime "end_datetime"
    t.text     "notes"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "work_type_id"
  end

  add_index "work_type_items", ["job_id"], name: "index_work_type_items_on_job_id"
  add_index "work_type_items", ["user_id"], name: "index_work_type_items_on_user_id"
  add_index "work_type_items", ["work_type_id"], name: "index_work_type_items_on_work_type_id"

  create_table "work_types", force: true do |t|
    t.string   "name"
    t.integer  "average_duration"
    t.text     "notes"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
