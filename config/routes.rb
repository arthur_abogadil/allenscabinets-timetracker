AllenscabinetsTimetracker::Application.routes.draw do
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root :to => "admin#index"




  get '/login' => 'utilities#login', as: :new_login
  post '/login' => 'utilities#login'
  
  get '/login2/:id' => 'utilities#login2'
  post '/login2/:id' => 'utilities#login2'


  post '/login3' => 'utilities#login3'
  

  get '/logout' => 'utilities#logout', as: :logout

  #USERS ---------------------------------------------------------
  get '/users' => 'admin#users'
  
  get '/users/add' => 'admin#users_add'
  get '/users/:id/edit' => 'admin#users_edit'
  post '/users/add' => 'admin#users_add'
  post '/users/:id/edit' => 'admin#users_edit'
  patch '/users/add' => 'admin#users_add'
  patch '/users/:id/edit' => 'admin#users_edit'

  get '/users/:id/view' => 'admin#users_view'
  get '/users/:id/delete' => 'admin#users_delete'
  get '/users/:id/delete_confirm' => 'admin#users_delete_confirm'
  #END USERS ------------------------------------------------------

  
  #WORK TYPES -----------------------------------------------------
  get '/worktypes' => 'admin#worktypes'
  get '/worktypes/add' => 'admin#worktypes_add'
  get '/worktypes/:id/edit' => 'admin#worktypes_edit'
  post '/worktypes/add' => 'admin#worktypes_add'
  post '/worktypes/:id/edit' => 'admin#worktypes_edit'
  patch '/worktypes/add' => 'admin#worktypes_add'
  patch '/worktypes/:id/edit' => 'admin#worktypes_edit'

  get '/worktypes/:id/view' => 'admin#worktypes_view'
  get '/worktypes/:id/delete' => 'admin#worktypes_delete'
  get '/worktypes/:id/delete_confirm' => 'admin#worktypes_delete_confirm'
  #END WORK TYPES -------------------------------------------------

  #WORK TYPE TEMPLATES --------------------------------------------

  get '/worktype_templates' => 'admin#worktype_templates'
  get '/worktype_templates/add' => 'admin#worktype_templates_add'
  get '/worktype_templates/:id/edit' => 'admin#worktype_templates_edit'
  post '/worktype_templates/add' => 'admin#worktype_templates_add'
  post '/worktype_templates/:id/edit' => 'admin#worktype_templates_edit'
  patch '/worktype_templates/add' => 'admin#worktype_templates_add'
  patch '/worktype_templates/:id/edit' => 'admin#worktype_templates_edit'

  get '/worktype_templates/:id/view' => 'admin#worktype_templates_view'
  get '/worktype_templates/:id/delete' => 'admin#worktype_templates_delete'
  get '/worktype_templates/:id/delete_confirm' => 'admin#worktype_templates_delete_confirm'

  get '/worktype_templates/:id/addworktype' => 'admin#worktype_templates_add_work_type'
  post '/worktype_templates/:id/addworktype' => 'admin#worktype_templates_add_work_type'

  get '/worktype_templates/:id/editworktype' => 'admin#worktype_templates_edit_work_type'
  post '/worktype_templates/:id/editworktype' => 'admin#worktype_templates_edit_work_type'  
  patch '/worktype_templates/:id/editworktype' => 'admin#worktype_templates_edit_work_type'  

  get '/worktype_templates/:id/deleteworktype' => 'admin#worktype_templates_delete_work_type'
  get '/worktype_templates/:id/deleteworktype_confirm' => 'admin#worktype_templates_delete_confirm_work_type'

  

  #END WORK TYPE TEMPLATES ----------------------------------------

  #JOBS -----------------------------------------------------------
  get '/jobs' => 'admin#jobs_ongoing'
  get '/jobs/ongoing' => 'admin#jobs_ongoing'
  get '/jobs/pending' => 'admin#jobs_pending'
  get '/jobs/completed' => 'admin#jobs_completed'

  get '/jobs/add' => 'admin#jobs_add'
  post '/jobs/add' => 'admin#jobs_add'

  get '/jobs/:id/edit' => 'admin#jobs_edit'
  post '/jobs/:id/edit' => 'admin#jobs_edit'
  patch '/jobs/:id/edit' => 'admin#jobs_edit'

  get '/jobs/:id/view' => 'admin#jobs_view'
  get '/jobs/:id/delete' => 'admin#jobs_delete'
  get '/jobs/:id/delete_confirm' => 'admin#jobs_delete_confirm'

  get '/jobs/:id/addworkitem' => 'admin#jobs_addworkitem'
  post '/jobs/:id/addworkitem' => 'admin#jobs_addworkitem'

  get '/worktypeitems/:id/edit' => 'admin#jobs_editworkitem'
  post '/worktypeitems/:id/edit' => 'admin#jobs_editworkitem'
  patch '/worktypeitems/:id/edit' => 'admin#jobs_editworkitem'

  get '/worktypeitems/:id/delete' => 'admin#worktypeitems_delete'
  get '/worktypeitems/:id/delete_confirm' => 'admin#worktypeitems_delete_confirm'

  get '/workitems/open' => 'admin#workitems_open'
  get '/workitems/ongoing' => 'admin#workitems_ongoing'
  get '/workitems/completed' => 'admin#workitems_completed'

  #END JOBS -------------------------------------------------------
 


  #REPORTS --------------------------------------------------------

  get '/reports/per_job' => 'admin#reports_per_job'
  get '/reports/per_job/:id/view' => 'admin#reports_per_job_view'

  get '/reports/per_employee' => 'admin#reports_per_employee'
  get '/reports/per_employee/:id/select_date' => 'admin#reports_per_employee_select_date'
  post '/reports/per_employee/view' => 'admin#reports_per_employee_view'

  get '/reports/per_employee_per_job' => 'admin#reports_per_employee_per_job'
  get '/reports/per_employee_per_job/:id/select_date' => 'admin#reports_per_employee_per_job_select_date'
  post '/reports/per_employee_per_job/view' => 'admin#reports_per_employee_per_job_view'

  get '/reports/per_date/select_date' => 'admin#reports_per_date_select_date'
  post '/reports/per_date/view' => 'admin#reports_per_date_view'



  #END REPORTS ----------------------------------------------------
  
  #PDF ------------------------------------------------------------

  get '/pdf_test' => 'admin#pdf_test'

  #END PDF --------------------------------------------------------

  #STAFF FRONTEND -------------------------------------------------

  get '/staffs' => 'staff#index'
  get '/workitem/:id/stop' => 'staff#workitem_stop'
  get '/workitem/:id/stop_confirm' => 'staff#workitem_stop_confirm'

  get '/job/:id/details' => 'staff#job_details'
  get '/worktype/:id/details' => 'staff#worktype_details'
  get '/worktype/:id/start' => 'staff#worktype_start'
  get '/worktype/:id/start_confirm' => 'staff#worktype_start_confirm'

  get '/stats' => 'staff#stats'

  #END STAFF FRONTEND ---------------------------------------------


  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
